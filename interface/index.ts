export interface UserPayloadInterface {
    username: string;
    password: string;
}

export interface UserPayloadInterface2 {
    username: string;
    password: string;
}

export interface UserPayloadInterface2 {
    username: string;
    password: string;
}

export interface IBoard { //게시판 등록
  memberType: string
  memberId:number
  boardTitle: string
  username: string
  boardContent:string
}
export interface IBoardList { //게시판 등록
  "id":number, //id
  "memberType":string //권한"memberId": number
  "memberId":number //멤버 id
  "boardTitle": string //게시글 제목
  "username": string //게시글 작성자
  "boardContent": string // 등록일


}


export interface IClassSharing { //수강반등록 post
  teacherId: number
  subjectName: string
  dateStart: string
  dateEnd: string
  trainingType: string
  classTurn: number
  registerHuman: number
  totalHuman: number
  ncsLevel: number
}

export interface IClassDetail { //수강반 상세보기 GET
  teacherId:number //강사id
  subjectId:string //과정명
  dateStart: string //교육시작일
  dateEnd: string //교육종료일
  trainingType: string //교육유형
  classTurn: number //회차
  registerHuman: number //재적인원
  totalHuman: number //총인원수
  ncsLevel: number //ncs 수준
}

export interface IClassList { //수강반 리스트 GET
  teacherId: number
  subjectName: string
  dateStart: string
  dateEnd: string
  periodTime: number
  trainingType: string
  classTurn: number
  registerHuman: number
  totalHuman: number
  mainTeacher: string
  ncsLevel: number
}
export interface ITeacher { //강사 등록
  "imgSrc": string
  "teacherName": string
  "username": string
  "password": string
  "passwordRe": string
  "gender":string
  "dateBirth": string
  "phoneNumber":string
  "address": string
  "email": string

}


export interface ITeacherList {
  "id":number
  "imgSrc":string
  "teacherName":string
  "gender": string
  "dateBirth":string
  "email":string


}
export interface ITeacherListDetail { //강사상세보기 post
  "imgSrc": string
  "teacherName": string
  "username": string
  "gender":string
  "dateBirth": string
  "phoneNumber":string
  "address": string
  "email": string

}
export interface IAttendance {
  id : number
  dateStart: string
  dateEnd: string
  reason: string
  addFile: string
  attendanceType: string
}

export interface IAttendanceList {
  studentId:number
  attendanceType:string
  reason: string
  dateStart: string
  dateEnd: string
  approvalState: string
  dateRegister: string
}

export interface IAttendanceAll { //StudentAttendanceApproval 수강생 출결승인 리스트 보기
  id: number
  studentName: string
  studentAttendance:number
  studentOut:number
  studentLate:number
  studentAbsence:number
  studentSick:number
}

export interface IAttendanceAllOne {
  id: number
  subjectName:string
  studentName:string
  studentId:string
  attendanceType:string
  reason:string
  dateStart:string
  dateEnd:string
  approvalState:string
  dateRegister:string
}

export interface IAttendanceDetail {//수강생 출결 상세보기 StudentAttendance 부분 GET
  studentAttendance: number //총출석일
  myStudentAttendance:number //나의출석일
  studentOut:number //조퇴
  studentLate:number //지각
  studentAbsence:number //무단결석
  studentSick:number //병가
  attendanceRate:number //출석률
  subjectName:string //훈련과정명
  academeName:string //기관명
  datePeriod:string //훈련기관
  progressRate:string //훈련진행률
}

export interface ITrainingDetail { //훈련일지 등록 상세보기 GET
  teacherId: number, //강사id
  dateTraining: number, //훈련날짜
  registrationNum:string , //재적(명)
  attendanceNum: number, //출결(명)
  theory: number, //이론(시간)
  practice: number, //실습(시간)
  absentWho: string, //결석(사람)
  lateWho:string//지각(사람)
  earlyLeaveWho: string, //조퇴
  trainingContent: string, //훈련내용
  etc:string//기타사항
  dateRegister:string//등록일
}


export interface ITrainingList { //훈련일지 리스트 GET
  id: number, //id
  teacherId: number, //강사id
  dateTraining: string //훈련날짜
  registrationNum: number, //재적(명)
  absentWho: number, //결석(명)
  lateWho: number, //지각(명)
  earlyLeaveWho: number, //조퇴(명)
  dateRegister: string //등록일
}

 export interface IStudent{ //승인하기 수정
   studentId:number,
   dateStart:string,
   dateEnd:string,
   reason:string,
   addFile:string,
   attendanceType:string,
   approvalState:string,

}
export interface IApprovalState{ //수강생 상세보기에서 승인상태만 변경
  approvalState:string,


}



