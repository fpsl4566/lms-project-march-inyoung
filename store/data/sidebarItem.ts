export default [
  {
    isTitle: true,
    name: "Menu",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "Dashboard",
    url: "/",
    icon: "home",
    submenu: [],
  },
  {
    isTitle: true,
    name: "강사용",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "강사정보",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "강사등록",
        url: "/teacherpost/new-teacher",
        icon: "chevron-right",
        submenu: [],

      },
      {
        isTitle: false,
        name: "강사리스트",
        url: "/teacher/class-change/teacher-list",
        icon: "chevron-right",
        submenu: [],

      },
      {
        isTitle: false,
        name: "강사수정",
        url: "/teacher/class-change/teacher-list",
        icon: "chevron-right",
        submenu: [],

      },

    ],
  },
  {
    isTitle: false,
    name: "훈련관리",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "훈련일지",
        url: "/teacher/trainingregister/training-register",
        icon: "chevron-right",
        submenu: [],

      },
    ],
  },
  {
    isTitle: false,
    name: "평가관리",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "General",
        url: "/form/general",
        icon: "chevron-right",
        submenu: [],

      },
      {
        isTitle: false,
        name: "General",
        url: "/form/general",
        icon: "chevron-right",
        submenu: [],
      }
    ],
  },




  {
    isTitle: true,
    name: "학생용",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "학생정보",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "General",
        url: "/form/general",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  {
    isTitle: false,
    name: "수강관리",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "수강등록",
        url: "/teacher/class-change/class-registration",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "수강과정리스트",
        url: "/teacher/class-change/class-list",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "수강상세보기",
        url: "/teacher/class-change/[id]",
        icon: "chevron-right",
        submenu: [],
      },

    ],
  },
  {
    isTitle: false,
    name: "출결관리",
    key: "forms",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "출결관리",
        url: "/attendances/stu-att-app-list",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "출결수정",
        url: "/attendances/detail[id]",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },

  {
    isTitle: false,
    name: "시간표",
    key: "table",
    icon: "table",
    submenu: [
      {
        isTitle: false,
        name: "시간표",
        url: "/timetables/timetable",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "시간표 관리",
        url: "/timetables/timetablechange",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  {
    isTitle: true,
    name: "서류관리",
    url: "",
    icon: "",
    submenu: [],
  },
  {
    isTitle: false,
    name: "서류관리",
    url: "",
    icon: "sidebar",
    submenu: [

      {
        isTitle: false,
        name: "설문수정",
        url: "/profile",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  {
    isTitle: false,
    name: "게시판",
    url: "",
    icon: "sidebar",
    submenu: [

      {
        isTitle: false,
        name: "게시판 등록",
        url: "/teacher/board/board-registration[post]",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "게시판 상세보기",
        url: "/teacher/board/board-list[get]",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "게시판 수정",
        url: "/teacher/board/board-modify",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
  {
    isTitle: false,
    name: "Page",
    url: "",
    icon: "sidebar",
    submenu: [
      {
        isTitle: false,
        name: "Login",
        url: "/login",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Register",
        url: "/register",
        icon: "chevron-right",
        submenu: [],
      },
      {
        isTitle: false,
        name: "Profile",
        url: "/profile",
        icon: "chevron-right",
        submenu: [],
      },
    ],
  },
];
