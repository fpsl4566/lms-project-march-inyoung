import { defineStore } from 'pinia';
export const useTodosStore = defineStore('todos', {
  state: () => ({
    todos: [],
  }),
  actions: {
    async fetchTodos() {
      const token = useCookie('token');
      console.log(token.value)
      const { data }: any = await useFetch(
        'https://jsonplaceholder.typicode.com/todos', {
          method: 'GET',
          mode: 'no-cors',
          headers: {
            'Authorization': `Bearer ${token}`
          }
        });
      if (data.value) {
        this.todos = data.value;
      }
    },
    async delTodos(delId: number) {
      const token = useCookie('token');
      console.log(token.value)
      const { data }: any = await useFetch(
        `https://jsonplaceholder.typicode.com/todos/${delId}`, {
          method: 'DELETE',
          mode: 'no-cors',
          headers: {
            'Authorization': `Bearer ${token}`
          }
        });
    },
  },
});
