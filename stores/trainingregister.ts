import type {ITrainingDetail, ITrainingList} from "~/interface";



export const useTrainingStore = defineStore('trainingregister', {
  state: () => ({
    trainingGetList: [] as ITrainingList[],
    TrainingRegister:[] as ITrainingDetail[],

  }),

  actions: {
    async getTrainingList() {

      const {data}: any = await useFetch(`http://34.64.218.128/:8080/v1/trainingRegister/all/subjectId/1`, {
        method: 'GET',

      });
      if (data) {
        this.trainingGetList = data.value.list;
        console.log(1111)
      }
    },
    async getTrainingListDetail(detailId: string) {
      const { id } = useRoute().params
      const {data}: any = await useFetch(`http://34.64.218.128/:8080/v1/studentAttendance/detail/student/${id}`,  {
        method: 'GET',

      });
      if (data) {
        console.log(id,"--------------이건 스토어------------------")
        console.log(data.value.data)
        console.log("----------------여기까지 스토어 값----------------")
        this.TrainingRegister = data.value.data;
      }
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTrainingStore, import.meta.hot))
}
