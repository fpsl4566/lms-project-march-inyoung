import type {IApprovalState, IAttendance, IAttendanceAllOne, IStudent, ITeacher} from "~/interface";


export const useAttendanceStore = defineStore('auth', {
  state: () => ({
    attendanceListAll: [] as IAttendance[],//수강생 출결 승인 리스트
    ListAllId:[] as IAttendanceAllOne[], //상세보기
    Attendance:[] as IStudent[],
    currentPage: 1,
    pageSize:10,
    totalItems:0,
    totalCount:0.

  }),

  actions: {
    async getListAll(state:string) {
      const {data}: any = await useFetch(`http://34.64.218.128:8080/v1/studentapproval/all/${state}`, {
        method: 'GET',

      });
      if (data) {
        this.attendanceListAll = data.value.list;
        console.log(state);

        console.log("------데이터 오는가?-----")
      }
    },

    // 페이징 처리
    async getListPaging(pageNum:number) {
      const {data}: any = await useFetch(`http://34.64.218.128:8080/v1/studentapproval/all/pageNum/${pageNum}`,
        {
          method: 'GET',

        });
      if (data) {
        this.attendanceListAll = data.value.list;
        console.log("------페이징 확인?-----")
      }
    },
    //수강생 출결 상세보기
    async getListDetail() {
      const { id } = useRoute().params
      const {data}: any = await useFetch(`http://34.64.218.128:8080/v1/studentapproval/detail/${id}`,  {
        method: 'GET',


      });
      if (data) {
        console.log(id,"--------------상세보기------------------")
        console.log("----------------여기까지 스토어 값----------------")
        this.ListAllId = data.value.data
        console.log(data.value)
      }
    },
    putAttendanceChang(data:IStudent) {
      console.log("-------여기까지 주소------")
      useFetch( `http://34.64.218.128:8080/v1/studentapproval/changeInfo/student-attendance-approvalId/1`, {
        method: 'PUT',
        body: data
      });
      console.log("-------주소 다음------")
    },
    putAttendanceApproval(data:number) {
      console.log("-------여기까지 주소------")
      useFetch( `http://34.64.218.128:8080/v1/studentapproval/change-approval/student-attendance-approvalId/${data}`, {
        method: 'PUT',
      });
      console.log("-------주소 다음------")
    },
  },


})
if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useAttendanceStore, import.meta.hot))
}
