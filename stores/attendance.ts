import type {IAttendanceAll, IAttendanceDetail} from "~/interface";


export const useTenDanceStore = defineStore('attendance', {
  state: () => ({
    attendanceList: [] as IAttendanceAll[],//수강생 출결 리스트 보기
    StudentAttendances:[] as IAttendanceDetail[], //수강생 출결관리 상세보기
  }),

  actions: {
    async getTeacherList() {

      const {data}: any = await useFetch('http://34.64.218.128/:8080/v1/studentAttendance/all/', {
        method: 'GET',

      });
      if (data) {
        this.attendanceList = data.value.list;
        console.log(1111)
      }
    },
    async getStudentattenDance(detailId: string) {
      const { id } = useRoute().params
      const {data}: any = await useFetch(`http://34.64.218.128/:8080/v1/studentAttendance/detail/student/${1}`,  {
        method: 'GET',


      });
      if (data) {
        console.log(id,"--------------이건 스토어------------------")
        console.log("----------------여기까지 스토어 값----------------")
        this.StudentAttendances = data.value.data.list;
        console.log(data.value)
      }
    }
  }
})
if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTenDanceStore, import.meta.hot))
}
