
export default defineNuxtConfig({

  app: {
    pageTransition: { name: "page", mode: "out-in" },
  },
  css: ["~/assets/css/main.css"],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  typescript: {
    shim: false,
    strict: true,
  },
  runtimeConfig: {
    apiSecret: "",
    public: {
      apiBase: "/api",
    },
  },
  modules: [
    "@pinia/nuxt",
    "@pinia-plugin-persistedstate/nuxt",
    "@kevinmarrec/nuxt-pwa",
    "@vueuse/nuxt",
    '@element-plus/nuxt',
    'nuxt-3-axios',

  ],
  pwa: {
    workbox: {
      enabled: false,
    },
  },
  axios:{
    baseURL: 'https://swapi.dev/api/'
    //options to pass Axios Config
  }
});

