# Nuxt 3 Admin Template

Open source Nuxt 3 admin template

![image](https://github.com/WailanTirajoh/nuxt3-admin-template/assets/53980548/b5d26e75-31f3-4308-9d36-7d1395ff0f66)


## Installation
```
# Clone this repository
git clone https://github.com/WailanTirajoh/nuxt3-admin-template.git

# Go to directory
cd nuxt3-admin-template

# Run with npm
npm install
npm run dev

# Or with pnpm
pnpm install --shamefully-hoist
pnpm dev

# Install Husky
npx husky install
```

## Preview
url: https://nuxt3-admin-template.vercel.app

## Docs
vue3-tailwind docs: https://vue3-tailwind-docs.vercel.app/guide/getting-started.html



# LMS 프로젝트_★
## 팀명 : March_
### 팀원 :
#### 안녕
##### 안녕


줄바꾸기


엔터 두번ㅊ치기 
구분선
# 프로젝트 소개 
*** // 선 

* 동그라미
* 만드는 법 

이름강조
**안녕**안녕
___저는___ 가울기 언더바 3개 

~~물결~~ 
*** 선

* 링크달기 [바로가기](주소 넣기)

![느낌표로 시작](./주소/주소) 넣는 방법 

1. 프로젝트 소개
2. 개발기간
3. 개발자 소개
4. 기술스택
5. 프로젝트 주요 기능
6. 프로젝트 아키텍처
7. 릴리즈 노트 (피드백 받은 내용 적기 )
